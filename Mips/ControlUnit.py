from Mips import Instruction
from Mips.InstructionFormat import InstructionFormat
from Mips.utils import convert_to_logic_format, opcodes, bits_to_logic_gates
from circuits.combinational.comparator import Comparator
from gate.and_gate import And
from gate.not_gate import Not
from gate.one_gate import One
from gate.or_gate import Or
from gate.zero_gate import Zero


class ControlUnit:
    reg_dst = Zero()
    alu_src = Zero()
    mem_to_reg = Zero()
    reg_write = Zero()
    mem_read = Zero()
    mem_write = Zero()
    branch = Zero()
    alu_op0 = Zero()
    alu_op1 = Zero()
    pc_src = Zero()
    jump = Zero()

    @staticmethod
    def initialize(opcode: str):
        opcode = bits_to_logic_gates(opcode, 6)
        op5 = opcode[0]
        op4 = opcode[1]
        op3 = opcode[2]
        op2 = opcode[3]
        op1 = opcode[4]
        op0 = opcode[5]

        not_op5 = Not(op5, "not_op5")
        not_op4 = Not(op4, "not_op4")
        not_op3 = Not(op3, "not_op3")
        not_op2 = Not(op2, "not_op2")
        not_op1 = Not(op1, "not_op1")
        not_op0 = Not(op0, "not_op0")

        r_format = And((not_op5, not_op4, not_op3, not_op2, not_op1, not_op0))
        lw = And((op5, not_op4, not_op3, not_op2, op1, op0))
        sw = And((op5, not_op4, op3, not_op2, op1, op0))
        beq = And((not_op5, not_op4, not_op3, op2, not_op1, not_op0))

        ControlUnit.reg_dst = r_format
        ControlUnit.alu_src = Or((lw, sw))
        ControlUnit.mem_to_reg = lw
        ControlUnit.reg_write = Or((r_format, lw))
        ControlUnit.mem_read = lw
        ControlUnit.mem_write = sw
        ControlUnit.branch = beq
        ControlUnit.alu_op1 = r_format
        ControlUnit.alu_op0 = beq

    @staticmethod
    def set_control_signals(depend=[]):
        ControlUnit.reg_dst.logic(depend)
        ControlUnit.alu_src.logic(depend)
        ControlUnit.mem_to_reg.logic(depend)
        ControlUnit.reg_write.logic(depend)
        ControlUnit.mem_read.logic(depend)
        ControlUnit.mem_write.logic(depend)
        ControlUnit.branch.logic(depend)
        ControlUnit.alu_op1.logic(depend)
        ControlUnit.alu_op0.logic(depend)




    # def set_control_signals(self):
    #     logic_format = self.instruction.encoded
    #     opcode = logic_format[:6]
    #     lw_opcode = convert_to_logic_format(opcodes["lw"], 6)
    #     sw_opcode = convert_to_logic_format(opcodes["sw"], 6)
    #     beq_opcode = convert_to_logic_format(opcodes["beq"], 6)
    #     if Comparator((opcode, lw_opcode)).logic():
    #         print("lw")
    #         self.mem_read = One()
    #         self.alu_src = One()
    #         self.mem_to_reg = One()
    #         self.reg_write = One()
    #         return
    #
    #     if Comparator((opcode, sw_opcode)).logic():
    #         print("sw")
    #         self.mem_write = One()
    #         self.alu_src = One()
    #         return
    #
    #     if Comparator((opcode, beq_opcode)).logic():
    #         print("beq")
    #         self.branch = One()
    #         self.alu_op0 = One()
    #         return
    #
    #     if self.instruction.format == InstructionFormat.R_TYPE:
    #         print("r_type")
    #         self.reg_dst = One()
    #         self.reg_write = One()
    #         self.alu_op1 = One()
    #         return
    #     if self.instruction.format == InstructionFormat.I_TYPE:
    #         print("i_type")
    #         self.reg_write = One()
    #         self.alu_src = One()
