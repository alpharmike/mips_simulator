from gate.and_gate import And
from gate.not_gate import Not
from gate.or_gate import Or


class ALUControlUnit:
    def __init__(self, alu_op, funct, name="ALUControlUnit"):
        self.alu_op = alu_op
        self.funct = funct
        self.name = name

        self.outputs = None

        self.build()

    def build(self):
        alu_op1 = self.alu_op[0]
        alu_op0 = self.alu_op[1]

        funct5 = self.funct[0]
        funct4 = self.funct[1]
        funct3 = self.funct[2]
        funct2 = self.funct[3]
        funct1 = self.funct[4]
        funct0 = self.funct[5]

        not_op0 = Not(alu_op0, name="Not_Op0")
        not_op1 = Not(alu_op1, name="Not_Op1")
        not_funct2 = Not(funct2, name="Not_Funct2")

        and0 = And((alu_op1, funct1))

        or0 = Or((funct3, funct0))

        op0 = And((alu_op1, or0))
        op1 = Or((not_op1, not_funct2))
        op2 = Or((and0, alu_op0))
        # op3 = And((alu_op0, not_op0))

        self.outputs = [op2, op1, op0]

    def logic(self, depend=[]):
        for output in self.outputs:
            output.logic(depend + [self])

        return self.outputs
