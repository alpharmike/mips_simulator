from enum import Enum


class InstructionFormat(Enum):
    R_TYPE = 1
    I_TYPE = 2
    J_TYPE = 3
