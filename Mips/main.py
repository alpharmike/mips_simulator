import itertools

from Mips.ALU import ALU
from Mips.ALUControlUnit import ALUControlUnit
from Mips.ControlUnit import ControlUnit
from Mips.DataMemory import DataMemory
from Mips.DestSelector import DestSelector
from Mips.Instruction import Instruction
from Mips.InstructionMemory import InstructionMemory
from Mips.RegisterFile import RegisterFile
from Mips.utils import bits_to_gates, convert_to_logic_format, bits_to_logic_gates
from adder.full_adder import FullAdder
from adder.incrementer import Incrementer
from adder.n_bit_adder import NBitAdder
from circuits.combinational.comparator import Comparator
from comparator.comparator import Comparator
from decoder.decoder_mxn import Decoder_nxm
from flipflop.d import D_FlipFlop
from gate.and_gate import And
from gate.input_gate import Input
from gate.one_gate import One
from gate.zero_gate import Zero
from multiplexer.mux2x1 import Mux2x1
from multiplexer.mux_mxn import Mux_mxn
from runner.circuit_runner import CircuitRunner
from shifter.left_shifter import LeftShifter
from shifter.n_bit_left_shifter import NBitLeftShifter
from shifter.n_bit_right_shifter import NBitRightShifter
from shifter.right_shifter import RightShifter
from signals.signal import Signal
from subtractor.n_bit_subtractor import NBitSubtractor

if __name__ == '__main__':
    # inst_line = "lw $t2, 25($t1)"
    # inst = Instruction(inst_line)
    # print(inst.encoded)
    # bits = inst.logic_format()
    # for bit in bits:
    #     print(bit.logic())

    clock = Signal(init_val=Zero())  # Clock for pulse
    # # #
    # a2 = D_FlipFlop(clock, One(), "a2")  # make our D flipflop
    # a2.set_input(One())  # set not for flipflop input
    # a2.set()
    #
    a1 = D_FlipFlop(clock, Zero(), "a1")  # make our D flipflop
    a1.set_input(Zero())  # set not for flipflop input
    a1.reset()

    a0 = D_FlipFlop(clock, One(), "a0")  # make our D flipflop
    a0.set_input(One())  # set not for flipflop input
    a0.set()
    #
    # b2 = D_FlipFlop(clock, Zero(), "b2")  # make our D flipflop
    # b2.set_input(Zero())  # set not for flipflop input
    # b2.reset()
    #
    b1 = D_FlipFlop(clock, Zero(), "b1")  # make our D flipflop
    b1.set_input(Zero())  # set not for flipflop input
    b1.reset()
    #
    b0 = D_FlipFlop(clock, One(), "b0")  # make our D flipflop
    b0.set_input(One())  # set not for flipflop input
    b0.set()
    #
    # comp = Comparator(([a1, a0], [b1, b0]))
    # print(comp.logic())

    # inst_line = "lw $t2, 25($t4)"
    # inst = Instruction(inst_line)
    # enc = ""
    # for bit in inst.encoded:
    #     enc += str(bit.q())

    # print(enc)
    # control = ControlUnit(inst)
    #
    # print(control.reg_write)
    # print(control.branch)
    # print(control.mem_write)
    # print(control.mem_read)
    # print(control.reg_dst)

    # a = [a2, a1, a0]
    # b = [b2, b1, b0]
    #
    # sub = NBitSubtractor(3, a, b, clock)
    # res = sub.logic()

    # inputs = [Input() for _ in range(4)]
    # dec = Decoder_nxm(inputs, One(), 4)

    # bits_to_gates("0011", inputs)
    # dec.logic()
    # for i in range(2 ** 5):
    #     dec.outputs[i].logic()
    # print(dec.outputs)
    # print("".join([str(o.output) for o in dec.outputs]))

    # mux = Mux_mxn((One(), Zero(), One(), Zero()), [Zero(), One()], 2)
    # mux = Mux2x1((b1, b0), [One()])
    # print(mux.logic())

    # rt = "10011"
    # rd = "00011"
    #
    # rt_bits = convert_to_logic_format(rt, 5)
    # rd_bits = convert_to_logic_format(rd, 5)
    # reg_dest = (One())
    #
    # dest = []
    #
    # for i in range(len(rt_bits)):
    #     mux = Mux2x1((rt_bits[i].output, rd_bits[i].output), [reg_dest])
    #     dest.append(mux.logic())
    #
    # print(dest)

    # opcode = '100011' # for lw
    # ControlUnit.initialize(opcode)
    # ControlUnit.set_control_signals()
    # print(ControlUnit.mem_read)

    opcode = '000000'
    ControlUnit.initialize(opcode)
    ControlUnit.set_control_signals()

    alu_op1 = ControlUnit.alu_op1
    alu_op0 = ControlUnit.alu_op0
    #
    acu = ALUControlUnit((alu_op1, alu_op0), (One(), Zero(), Zero(), Zero(), Zero(), Zero()))
    acu_out = acu.outputs
    # print(acu_out.logic())

    # a = [One(), Zero(), One(), Zero()]
    # b = [Zero(), Zero(), One(), Zero()]
    #
    # adder = NBitAdder(a, b, 4, clock)
    # print(adder.logic())

    # a = [One(), One(), One(), Zero()]
    # b = [One(), One(), One(), Zero()]
    #
    # c = Comparator((a, b), 4)
    #
    # print(c.logic())

    # inputs = [One(), Zero(), Zero(), One()]
    # dec = Decoder_nxm(inputs, One(), 4)

    # bits_to_gates("0011", inputs)
    # dec.logic()
    # print(dec.logic())
    # print("".join([str(o.output) for o in dec.outputs]))

    # RegisterFile.initialize(clock)

    # reg_file = RegisterFile(clock, [Zero(), Zero(), One(), Zero(), One()], [Zero(), Zero(), One(), Zero(), Zero()], [])
    # a, b = reg_file.logic()
    # print(b)

    rt = [Zero(), One(), One(), Zero(), One()]
    rd = [Zero(), Zero(), Zero(), One(), Zero()]


    # alu = ALU(([a1, a0], [b1, b0]), acu_out, clock)
    # print(alu.logic())

    s = rt

    t = NBitLeftShifter(s, One(), 2)

    # print("s_2")
    # s = t.logic()
    # print(s)
    # t = LeftShifter(s, One())
    # print(t.logic())

    # d = DestSelector(rt, rd)

    # print(d.logic())
    # a = [Zero() for i in range(7)]
    # a[-5] = One()
    # i = InstructionMemory(clock, a)
    # print(i.logic())
    # print(i.logic())
    # a = [b1, b0]
    # for i in range(2):
    #     a = Incrementer(a)
    #     a = a.logic()
    #
    # print(a)

    #
    a = [Zero() for i in range(32)]
    b = [Zero() for j in range(32)]
    a[-1] = One()
    a[-2] = One()
    b[-1] = One()
    b[-2] = One()

    alu = ALU((a, b), acu_out, clock)

    # print(alu.logic())
    # alu.logic()
    ControlUnit.reg_write = One()
    # data = [Input() for i in range(32)]
    # data[-1].output = 1
    # data[-2].output = 1
    # data.append(One())

    reg = RegisterFile(clock, rt, rd, rd, alu.output)
    # reg.write_data(data)
    # print(RegisterFile.registers[2].bits)

    # reg.read_data1()
    # reg.read_data2()
    #
    # a, b = reg.logic()
    #
    # print(a)
    # reg.logic()
    # print(reg.rd2)
    a = CircuitRunner.run([reg, alu], clock, 32)
    print(a[0].rd2)
    print(reg.registers[2].output)
    print(alu.output)
    # rd1, rd2 = reg.logic()
    # print(reg.registers[2].inputs)
    # print(print(rd1))

    # print(bits_to_logic_gates(str(bin(12)), 5))

    # a = And((One(), One()))
    # d_1 = D_FlipFlop(None, a)
    # d_1.set_input(a)
    #
    # print(a.logic())


