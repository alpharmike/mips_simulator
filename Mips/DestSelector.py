from Mips.ControlUnit import ControlUnit
from multiplexer.mux2x1 import Mux2x1


class DestSelector:
    def __init__(self, rt, rd):
        self.rt = rt
        self.rd = rd
        self.output = None

        self.build()

    def build(self):
        dest = []
        for i in range(len(self.rt)):
            mux = Mux2x1((self.rt[i], self.rd[i]), [ControlUnit.reg_dst])
            dest.append(mux)

        self.output = dest

    def logic(self, depend=[]):
        for o in self.output:
            o.logic(depend + [self])

        return self.output
