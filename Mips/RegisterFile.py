from typing import List

from Mips.ControlUnit import ControlUnit
from Mips.utils import register_table, bits_to_gates, convert_to_logic_format, bits_to_logic_gates
from comparator.comparator import Comparator
from componenets.register import Register
from decoder.decoder_mxn import Decoder_nxm
from flipflop.d import D_FlipFlop
from gate.and_gate import And
from gate.input_gate import Input
from gate.one_gate import One
from gate.zero_gate import Zero
from multiplexer.mux2x1 import Mux2x1
from multiplexer.mux_mxn import Mux_mxn


class RegisterFile:
    def __init__(self, clock, rr1, rr2, wr, wd):
        self.rr1 = rr1
        self.rr2 = rr2
        self.wr = wr
        self.wd = wd
        self.registers = []
        self.clock = clock

        self.rd1 = None
        self.rd2 = None
        self.output = None

        self.build()

    def build(self):
        print("write data: " + str(self.wd))
        for i in range(32):
            self.registers.append(Register(self.clock, [Input() for c in range(32)]))

        dec = Decoder_nxm(self.wr, ControlUnit.reg_write, 5)
        muxs = []
        for i in range(len(dec.outputs)):
            for j in range(32):
                mux = Mux2x1((self.registers[j].output[i], self.wd[i]), [dec.outputs[j]])
                self.registers[j].inputs[i] = mux
                muxs.append(mux)

        self.output = muxs

        rd1_muxs = []
        for i in range(32):
            mux_inputs = []
            for j in range(32):
                mux_inputs.append(self.registers[j].inputs[i])

            rd1_muxs.append(Mux_mxn(mux_inputs, self.rr1, 5))

        self.rd1 = rd1_muxs

        rd2_muxs = []
        for i in range(32):
            mux_inputs = []
            for j in range(32):
                mux_inputs.append(self.registers[j].inputs[i])

            rd2_muxs.append(Mux_mxn(mux_inputs, self.rr2, 5))

        self.rd2 = rd2_muxs

    def logic(self, depend=[]):
        for o in self.output:
            o.logic(depend + [self])

        for o in self.rd1:
            o.logic(depend + [self])

        for o in self.rd2:
            o.logic(depend + [self])

        return self.rd1, self.rd2

    def set_rr1(self, rr1):
        self.rr1 = rr1

    def set_rr2(self, rr2):
        self.rr1 = rr2

    def set_wr(self, wr):
        self.wr = wr

    def set_wd(self, wd):
        self.wd = wd
