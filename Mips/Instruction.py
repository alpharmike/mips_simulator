from circuits.combinational.comparator import Comparator
from gate.and_gate import And
from gate.not_gate import Not
from Mips.utils import opcodes, functs, r_types, i_types, j_types, register_table, convert_to_logic_format
from Mips.InstructionFormat import InstructionFormat
from flipflop.d import D_FlipFlop
from gate.or_gate import Or
from signals.signal import Signal
from adder.full_adder import FullAdder
from gate.zero_gate import Zero
from gate.one_gate import One


class Instruction:
    def __init__(self, representation: str):
        self.representation = representation
        self.tokenized = self.tokenize()
        self.opcode = []
        self.rs = ""
        self.rt = ""
        self.rd = ""
        self.shamt = "00000"
        self.funct = []
        self.format = ""
        self.encoded = ""
        self.immediate = ""

        self.set_opcode()
        self.set_format()
        self.set_funct()
        self.parse_registers()
        self.set_shamt()
        self.set_immediate()

        self.encode()

    def tokenize(self):
        cleared = self.representation.replace(",", " ")
        whitespace_removed = cleared.split(" ")
        while '' in whitespace_removed:
            whitespace_removed.remove('')
        print(whitespace_removed)
        return whitespace_removed

    def set_opcode(self):
        instruction = self.tokenized[0]
        # self.opcode = format(opcodes[instruction], '06b')
        self.opcode = convert_to_logic_format(opcodes[instruction], 6)
        print(self.opcode)

    def set_format(self):
        instruction = self.tokenized[0]
        if instruction in r_types:
            self.format = InstructionFormat.R_TYPE
        elif instruction in i_types:
            self.format = InstructionFormat.I_TYPE
        elif instruction in j_types:
            self.format = InstructionFormat.J_TYPE

    def set_funct(self):
        instruction = self.tokenized[0]
        # self.funct = format(functs[instruction], '06b') if instruction in functs else ""
        self.funct = convert_to_logic_format(functs[instruction], 6) if instruction in functs else []

    def set_immediate(self):
        lw_opcode = convert_to_logic_format(opcodes["lw"], 6)
        sw_opcode = convert_to_logic_format(opcodes["sw"], 6)

        lw_or_sw = Or((Comparator((self.opcode, lw_opcode)).logic(), Comparator((self.opcode, sw_opcode)).logic()))
        if lw_or_sw.logic():
            self.immediate = convert_to_logic_format(int(self.parse_wrapped_offset()), 16)

        elif self.format == InstructionFormat.I_TYPE:
            self.immediate = convert_to_logic_format(int(self.parse_address(), 16), 16)

        # if self.opcode == format(opcodes['lw'], '05b') or self.opcode == format(opcodes['sw'], '05b'):
        #     self.immediate = format(int(self.parse_wrapped_offset()), '016b')
        #
        # elif self.format == InstructionFormat.I_TYPE:
        #     self.immediate = format(int(self.parse_address(), 16), '016b')

    def set_shamt(self):
        sll_opcode = convert_to_logic_format(opcodes["sll"], 6)
        srl_opcode = convert_to_logic_format(opcodes["srl"], 6)
        sll_funct = convert_to_logic_format(functs["sll"], 6)
        srl_funct = convert_to_logic_format(functs["srl"], 6)

        sll_and = And((Comparator((self.opcode, sll_opcode)).logic(), Comparator((self.funct, sll_funct)).logic()))
        srl_and = And((Comparator((self.opcode, srl_opcode)).logic(), Comparator((self.funct, srl_funct)).logic()))
        srl_or_sll = Or((sll_and, srl_and))
        if srl_or_sll.logic():
            self.shamt = convert_to_logic_format(int(self.tokenized[3]), 5)
        elif self.format == InstructionFormat.R_TYPE:
            self.shamt = convert_to_logic_format(0, 5)

        # if self.opcode == format(opcodes['sll'], '06b') or self.opcode == format(opcodes['srl'], '06b'):
        #     self.shamt = format(int(self.tokenized[3]), '05b')
        # elif self.format == InstructionFormat.R_TYPE:
        #     self.shamt = "00000"

    def parse_registers(self):
        lw_opcode = convert_to_logic_format(opcodes["lw"], 6)
        sw_opcode = convert_to_logic_format(opcodes["sw"], 6)
        lw_or_sw = Or((Comparator((self.opcode, lw_opcode)).logic(), Comparator((self.opcode, sw_opcode)).logic()))

        if lw_or_sw.logic():
            self.rs = convert_to_logic_format(register_table[self.parse_wrapped_register()], 5)
            self.rt = convert_to_logic_format(register_table[self.tokenized[1]], 5)

        sll_opcode = convert_to_logic_format(opcodes["sll"], 6)
        srl_opcode = convert_to_logic_format(opcodes["srl"], 6)
        sll_funct = convert_to_logic_format(functs["sll"], 6)
        srl_funct = convert_to_logic_format(functs["srl"], 6)

        sll_and = And((Comparator((self.opcode, sll_opcode)).logic(), Comparator((self.funct, sll_funct)).logic()))
        srl_and = And((Comparator((self.opcode, srl_opcode)).logic(), Comparator((self.funct, srl_funct)).logic()))
        srl_or_sll = Or((sll_and, srl_and))

        if srl_or_sll.logic():
            self.rs = convert_to_logic_format(0, 5)
            self.rt = convert_to_logic_format(register_table[self.tokenized[2]], 5)
            self.rd = convert_to_logic_format(register_table[self.tokenized[1]], 5)

        elif self.format == InstructionFormat.R_TYPE:
            self.rs = convert_to_logic_format(register_table[self.tokenized[2]], 5)
            self.rt = convert_to_logic_format(register_table[self.tokenized[3]], 5)
            self.rd = convert_to_logic_format(register_table[self.tokenized[1]], 5)
        elif self.format == InstructionFormat.I_TYPE:
            self.rs = convert_to_logic_format(register_table[self.tokenized[2]], 5)
            self.rt = convert_to_logic_format(register_table[self.tokenized[1]], 5)

        # if self.opcode == format(opcodes['lw'], '06b') or self.opcode == format(opcodes['sw'], '06b'):
        #     self.rs = format(register_table[self.parse_wrapped_register()], '05b')
        #     self.rt = format(register_table[self.tokenized[1]], '05b')
        # elif self.opcode == format(opcodes['sll'], '06b') or self.opcode == format(opcodes['srl'], '06b'):
        #     self.rs = "00000"
        #     self.rt = format(register_table[self.tokenized[2]], '05b')
        #     self.rd = format(register_table[self.tokenized[1]], '05b')
        # elif self.format == InstructionFormat.R_TYPE:
        #     self.rs = format(register_table[self.tokenized[2]], '05b')
        #     self.rt = format(register_table[self.tokenized[3]], '05b')
        #     self.rd = format(register_table[self.tokenized[1]], '05b')
        # elif self.format == InstructionFormat.I_TYPE:
        #     self.rs = format(register_table[self.tokenized[2]], '05b')
        #     self.rt = format(register_table[self.tokenized[1]], '05b')

    def parse_wrapped_register(self):
        return self.representation[self.representation.find('(') + 1: self.representation.find(')')]

    def parse_wrapped_offset(self):
        return self.tokenized[2][0: self.tokenized[2].find('(')]

    def parse_address(self):
        address = ""
        if self.format == InstructionFormat.I_TYPE:
            address = self.tokenized[3]

        return address

    def encode(self):
        encoded_instruction = []
        encoded_instruction += self.opcode
        if self.format == InstructionFormat.R_TYPE:
            encoded_instruction += self.rs
            encoded_instruction += self.rt
            encoded_instruction += self.rd
            encoded_instruction += self.shamt
            encoded_instruction += self.funct

        lw_opcode = convert_to_logic_format(opcodes["lw"], 6)
        sw_opcode = convert_to_logic_format(opcodes["sw"], 6)
        lw_or_sw = Or((Comparator((self.opcode, lw_opcode)).logic(), Comparator((self.opcode, sw_opcode)).logic()))

        if self.format == InstructionFormat.I_TYPE or lw_or_sw.logic():
            encoded_instruction += self.rs
            encoded_instruction += self.rt
            encoded_instruction += self.immediate

        # elif self.format == InstructionFormat.I_TYPE or self.opcode == format(opcodes['lw'], '05b') or self.opcode == format(opcodes['sw'], '05b'):
        #     encoded_instruction += self.rs
        #     encoded_instruction += self.rt
        #     encoded_instruction += self.immediate

        self.encoded = encoded_instruction

    def logic_format(self):
        bits = []
        for bit in self.encoded:
            clock = Signal()  # Clock for pulse

            if int(bit):
                d_input = One()
            else:
                d_input = Zero()

            d_flip_flop = D_FlipFlop(clock, d_input)
            d_flip_flop.set_input(d_input)
            d_flip_flop.set() if d_input.logic() else d_flip_flop.reset()
            bits.append(d_flip_flop)
        return bits
