from componenets.register import Register
from flipflop.d import D_FlipFlop
from gate.zero_gate import Zero


class Memory:

    def __init__(self, clock, address=[]):
        self.clock = clock
        self.storage = []
        self.address = address
        self.output = None

        self.initialize()
        self.build()

    def build(self):
        pass

    def initialize(self):
        for i in range(2 ** 2):
            byte = []
            for i in range(8):
                bit = D_FlipFlop(self.clock, Zero())
                bit.set_input(Zero())
                bit.reset()
                byte.append(bit)

            self.storage.append(byte)

    def set_address(self, address):
        self.address = address
