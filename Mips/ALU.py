from adder.n_bit_adder import NBitAdder
from gate.and_gate import And
from gate.one_gate import One
from gate.or_gate import Or
from gate.zero_gate import Zero
from multiplexer.mux_mxn import Mux_mxn
from subtractor.n_bit_subtractor import NBitSubtractor


class ALU:
    def __init__(self, inputs, alu_controls, clock, name="ALU"):
        self.inputs = inputs
        self.alu_control = alu_controls
        self.clock = clock
        self.output = None

        self.build()

    def build(self):

        add = NBitAdder(self.inputs[0], self.inputs[1], len(self.inputs[0]), self.clock).outputs
        sub = NBitSubtractor(self.inputs[0], self.inputs[1], len(self.inputs[0]), self.clock).outputs

        result_mux = []
        for i in range(len(self.inputs[0])):
            operations = (
                And((self.inputs[0][i], self.inputs[1][i])),
                Or(self.inputs[0][i], self.inputs[1][i]),
                add[i],
                And(self.inputs[0][i], self.inputs[1][i]),
                And(self.inputs[0][i], self.inputs[1][i]),
                And(self.inputs[0][i], self.inputs[1][i]),
                sub[i],
                And(self.inputs[0][i], self.inputs[1][i]),
            )
            result_mux.append(Mux_mxn(operations, self.alu_control, 3))

        self.output = result_mux

    def logic(self, depend=[]):
        for o in self.output:
            o.logic(depend + [self])

        return self.output
