from typing import List

from flipflop.d import D_FlipFlop
from gate.one_gate import One
from gate.zero_gate import Zero
from signals.signal import Signal

opcodes = {
    "add": 0x00,
    "sub": 0x00,
    "addi": 0x08,
    "and": 0x00,
    "or": 0x00,
    "xor": 0x00,
    'sll': 0x00,
    'srl': 0x00,
    "beq": 0x04,
    "lw": 0x23,
    "sw": 0x2B,
}

functs = {
    "add": 0x20,
    "sub": 0x22,
    "addi": 0x08,
    "and": 0x24,
    "or": 0x25,
    "xor": 0x26,
    'sll': 0x00,
    'srl': 0x02
}

register_table = {
    '$zero': 0,
    '$at': 1,
    '$v0': 2,
    '$v1': 3,
    '$a0': 4,
    '$a1': 5,
    '$a2': 6,
    '$a3': 7,
    '$t0': 8,
    '$t1': 9,
    '$t2': 10,  # format(10, '05b')
    '$t3': 11,
    '$t4': 12,
    '$t5': 13,
    '$t6': 14,
    '$t7': 15,
    '$s0': 16,
    '$s1': 17,
    '$s2': 18,
    '$s3': 19,
    '$s4': 20,
    '$s5': 21,
    '$s6': 22,
    '$s7': 23,
    '$t8': 24,
    '$t9': 25,
    '$k0': 26,
    '$k1': 27,
    '$gp': 28,
    '$sp': 29,
    '$fp': 30,
    '$ra': 31
}

r_types = [
    'add',
    'sub',
    'and',
    'xor',
    'sll',
    'srl'
]

i_types = [
    'beq',
    'addi',
]

j_types = [

]


def convert_to_logic_format(value, length) -> List[D_FlipFlop]:
    if not isinstance(value, str):
        converted_value = format(value, f"0{length}b")
    else:
        converted_value = value.zfill(length - len(value))
    stored_bits = []
    clock = Signal()

    for bit in converted_value:
        if int(bit):
            d_input = One()
        else:
            d_input = Zero()

        d_flip_flop = D_FlipFlop(clock, d_input)
        d_flip_flop.set_input(d_input)
        d_flip_flop.set() if d_input.logic() else d_flip_flop.reset()
        stored_bits.append(d_flip_flop)

    return stored_bits


def bits_to_gates(bit_string, inputs):
    for i in range(len(bit_string)):
        inputs[i].output = 0 if bit_string[i] == "0" else 1


def bits_to_logic_gates(bit_string, length):
    logic_gates = []
    for i in range(len(bit_string)):
        logic_gates.append(One() if bit_string[i] == "1" else Zero())

    for i in range(length - len(logic_gates)):
        logic_gates.insert(0, Zero())

    return logic_gates
