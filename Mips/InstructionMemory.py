import itertools

from Mips.Memory import Memory
from adder.incrementer import Incrementer
from adder.n_bit_adder import NBitAdder
from multiplexer.mux_mxn import Mux_mxn


class InstructionMemory(Memory):
    def __init__(self, clock, address):  # address is the program counter here
        super().__init__(clock, address)
        self.build()

    def build(self):
        memory_bits = list(itertools.chain.from_iterable(self.storage))
        print(memory_bits)
        pc = self.address
        bit_muxs = []
        for i in range(32):
            bit = Mux_mxn(memory_bits, pc[-5:], 5)
            bit_muxs.append(bit)
            pc = Incrementer(pc).logic()
            print(i)
        print(bit_muxs)

        self.output = bit_muxs

    def logic(self, depend=[]):
        for o in self.output:
            o.logic(depend + [self])

        return self.output
